
const SELECTORS = {
    toolbar: '#com.google.android.calculator:id/toolbar',
    digit_2: '~com.google.android.calculator:id/digit_2'
};

const CalculadoraPage = function(){

    this.esperarElementoSumir = function(){
        // const selector = 'new UiSelector().resourceId(`${SELECTORS.toolbar}`)';
        const Button = $(`android=${SELECTORS.toolbar}`);
        Button.waitForExist(5000,false);
        // $('~com.google.android.calculator:id/toolbar').waitForExist(5000,false);
    };

    this.selecionarNumero = function(num){
        browser.pause(2000);
        const selector = 'new UiSelector().resourceId("com.google.android.calculator:id/digit_'+num+'")';
        const tx = $(`android=${selector}`);
        tx.click();
       
    };

    this.selecionarBotaoMais=function(){
        const selector = 'new UiSelector().resourceId("com.google.android.calculator:id/op_add")';
        const Button = $(`android=${selector}`);
        Button.click();
    }
}
module.exports = new CalculadoraPage();